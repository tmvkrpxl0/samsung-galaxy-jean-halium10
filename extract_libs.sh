#!/bin/bash
echo "extracting.."

if [ ! -d /mnt/system ]; then
    echo "stock system partition is not mounted on /mnt/system"
    exit 1
fi

if [ ! -d /mnt/vendor ]; then
    echo "stock vendor partition is not mounted on /mnt/vendor"
    exit 1
fi

scp phablet@10.15.19.82:/data/logcat.txt ./

java -jar LibFilter.jar $(pwd)/logcat.txt $(pwd)/files_to_move

cat files_to_move | while read line
do
    if [ ! -z $line ]; then
        echo "copying $line"
        cp /mnt/system/system/lib/$line ./overlay/system/system/lib/$line
    fi
done

ssh phablet@10.15.19.82 "/home/phablet/test"

scp ./overlay/system/system/lib/* phablet@10.15.19.82:/home/phablet/test/
