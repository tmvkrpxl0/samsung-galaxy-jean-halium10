package com.tmvkrpxl0

import java.io.File

class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.size < 2){
                println("Usage: java -jar LibFilter.jar <input file> <output file>")
                println("Note: missing lib names will be appended to output file distinctly, so it can make full list of libs to add")
                return
            }
            val logcat = File(args[0]).readLines()
            val output = File(args[1])
            if (!output.exists())output.createNewFile()
            val libNames = output.readLines().toHashSet()
            val missing = logcat.filter { it.contains(".so\" not found") }
            libNames.addAll(missing.map { line ->
                line.substring(line.indexOf("library \"") + 9, line.indexOf("\" not found"))
            })
            val writer = output.bufferedWriter()
            libNames.forEach { name ->
                println(name)
                writer.write(name)
                writer.newLine()
            }
            writer.flush()
            writer.close()
        }
    }
}
